(ns zookareg.kafka-test
  (:require [clojure.spec.alpha :as s]
            [clojure.test :refer :all]
            [kafka-utils :as ku]
            [scribe.avro.core :as scribe]
            zookareg.core))

(s/def :kafka.Record/msg string?)
(s/def :kafka/Record
  (s/keys :req-un [:kafka.Record/msg]))
(def kafka-record-avro-schema
  (scribe/->avro-schema :kafka/Record))

(use-fixtures :once zookareg.core/with-zookareg-fn)

(def k-topic "mah-topic")

(deftest kafka-test
  (testing "can produce a record"
    (ku/with-1-partition-consumer-from-end  k-topic
      (fn [k-consumer]
        (ku/produce k-topic kafka-record-avro-schema {:msg "hi"})
        (let [got (ku/poll* k-consumer)]
          (is (not-empty got))
          (is (= {:msg "hi"} (first got))))))))
