(ns zookareg.core
  "Utilities for configuring, starting and halting Zookareg.

  A running Zookareg instance consists of:
  - a Kafka Broker
  - a Curator (Zookeeper) TestingServer
  - a Confluent Schema Registry"
  (:require [aero.core :as aero]
            [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [integrant.core :as ig]
            [me.raynes.fs :as fs]
            [systema.core :as sys]
            [zookareg.state :as state]))

(defmethod aero/reader 'fs/temp-dir
  [_ _ suffix]
  (.getAbsolutePath ^java.io.File (fs/temp-dir suffix)))

(defn read-default-config
  "Reads the default Zookareg config to be modified and passed to
  `init-zookareg` if needed."
  []
  (-> "zookareg.config.edn"
      io/resource
      sys/read-ig-config))

(defn halt-zookareg!
  "Halts the running Zookareg instance."
  []
  (when @state/state
    (swap! state/state
           (fn [s]
             (ig/halt! (:system s))
             nil))))

(defn init-zookareg
  "Starts a Zookareg instance."
  ([] (init-zookareg (read-default-config)))
  ([config]
   (log/info "starting ZooKaReg with config:" config)
   (try
     (halt-zookareg!)
     (ig/load-namespaces config)
     ;; TODO stick in the same atom!
     (reset! state/state
             {:system (ig/init config)
              :config config})
     (catch clojure.lang.ExceptionInfo ex
       ;; NOTE tears down partially initialised system
       (ig/halt! (:system (ex-data ex)))
       (throw (.getCause ex))))))

(defn with-zookareg-fn
  "Starts up zookareg with the specified configuration; executes the function then shuts down."
  ([config f]
   {:pre [(map? config) (fn? f)]}
   (try
     (init-zookareg config)
     (f)
     (finally
       (halt-zookareg!))))
  ([f]
   (with-zookareg-fn (read-default-config) f)))

(defmacro with-zookareg
  "Starts up zookareg with the specified configuration; executes the body then shuts down."
  [config & body]
  `(with-zookareg-fn ~config (fn [] ~@body)))
