(defproject vise890/zookareg "2.0.1-1"
  :description "Embedded ZOokeeper KAfka and Confluent's Schema REGistry"
  :url "http://gitlab.com/vise890/zookareg"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}

  :repositories {"confluent" "https://packages.confluent.io/maven"}

  :dependencies [[org.clojure/clojure "1.9.0"]
                 [integrant "0.7.0"]
                 [vise890/systema "2018.10.12"]

                 [org.clojure/tools.logging "0.4.1"]
                 [org.clojure/tools.namespace "0.2.11"]
                 [me.raynes/fs "1.4.6"]

                 [org.apache.curator/curator-test "4.0.1"]
                 [org.apache.kafka/kafka_2.12 "2.0.1"]
                 [io.confluent/kafka-schema-registry "5.0.1" :exclusions [org.apache.kafka/kafka-clients
                                                                          org.apache.kafka/kafka_2.11]]]

  :exclusions [[org.slf4j/slf4j-log4j12]]

  :plugins [[lein-codox "0.10.5"]
            [lein-nvd "0.5.6"]]

  :resource-paths ["resources"]

  :profiles {:dev  {:source-paths   ["dev/clj"]
                    :resource-paths ["dev/resources"]
                    :dependencies   [[org.slf4j/jcl-over-slf4j "1.7.25"]
                                     [org.slf4j/jul-to-slf4j "1.7.25"]
                                     [org.slf4j/log4j-over-slf4j "1.7.25"]
                                     [org.slf4j/slf4j-api "1.7.25"]
                                     [ch.qos.logback/logback-classic "1.2.3"]
                                     [ch.qos.logback/logback-core "1.2.3"]

                                     [vise890/scribe "2018.12.01" :exclusions [org.apache.avro/avro]]

                                     [org.apache.kafka/kafka-clients "2.0.1"]
                                     [ovotech/kafka-avro-confluent "1.1.1-6"]]}
             :test {:global-vars {*warn-on-reflection* true}}
             :ci   {:deploy-repositories [["clojars" {:url           "https://clojars.org/repo"
                                                      :username      :env
                                                      :password      :env
                                                      :sign-releases false}]]}})
